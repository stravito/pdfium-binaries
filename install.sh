#/usr/bin/bash

mkdir -p /usr/local/cmake
mkdir -p /usr/local/include/pdfium
cp -r staging/include/* /usr/local/include/pdfium
cp staging/lib/libpdfium.a /usr/local/lib
cp PDFiumConfig.cmake /usr/local/cmake/FindPDFium.cmake
